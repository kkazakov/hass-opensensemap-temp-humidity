# hass-opensensemap-temp-humidity

Home Assistant module for getting temperature and humidity from OpenSenseMap

Example usage:


```
sensor:
  - platform: osm_temp_humidity
    station_id: 5bf5281803d564001a960576
    name: Sofia
```
