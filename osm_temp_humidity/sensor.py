"""Support for openSenseMap Air Quality data."""
from datetime import timedelta
import logging

from opensensemap_api import OpenSenseMap
from opensensemap_api.exceptions import OpenSenseMapError
import voluptuous as vol

from homeassistant.components.air_quality import PLATFORM_SCHEMA, AirQualityEntity
from homeassistant.const import CONF_NAME, TEMP_CELSIUS, TEMP_FAHRENHEIT, PERCENTAGE
from homeassistant.exceptions import PlatformNotReady
from homeassistant.helpers.aiohttp_client import async_get_clientsession
import homeassistant.helpers.config_validation as cv
from homeassistant.util import Throttle
from homeassistant.helpers.entity import Entity

_LOGGER = logging.getLogger(__name__)

ATTRIBUTION = "Data provided by openSenseMap"

CONF_STATION_ID = "station_id"

SCAN_INTERVAL = timedelta(minutes=5)

PLATFORM_SCHEMA = PLATFORM_SCHEMA.extend(
    {vol.Required(CONF_STATION_ID): cv.string, vol.Optional(CONF_NAME): cv.string}
)


async def async_setup_platform(hass, config, async_add_entities, discovery_info=None):
    """Set up the openSenseMap air quality platform."""

    name = config.get(CONF_NAME)
    station_id = config[CONF_STATION_ID]

    session = async_get_clientsession(hass)
    osm_api = OpenSenseMapData(OpenSenseMap(station_id, hass.loop, session))

    await osm_api.async_update()

    if "name" not in osm_api.api.data:
        _LOGGER.error("Station %s is not available", station_id)
        raise PlatformNotReady

    station_name = osm_api.api.data["name"] if name is None else name

    async_add_entities([OpenSenseMapTemperature(station_name, osm_api)], True)
    async_add_entities([OpenSenseMapHumidity(station_name, osm_api)], True)


class OpenSenseMapTemperature(Entity):
    """Implementation of an openSenseMap temperature entity."""

    def __init__(self, name, osm):
        """Initialize the temperature entity."""
        self._name = name
        self._osm = osm
        self._icon = "mdi:thermometer"
        self._unit_of_measurement = TEMP_CELSIUS

    @property
    def icon(self):
        """Return the icon."""
        return self._icon

    @property
    def name(self):
        """Return the name of the temperature entity."""
        return f"{self._name}_temp"

    @property
    def attribution(self):
        """Return the attribution."""
        return ATTRIBUTION

    @property
    def state(self):
        """Return the state of the device."""
        if self._osm.api.temperature is not None:
            return self._osm.api.temperature

    @property
    def unit_of_measurement(self):
        """Return the unit of measurement of this entity, if any."""
        return self._unit_of_measurement

    async def async_update(self):
        """Get the latest data from the openSenseMap API."""
        await self._osm.async_update()




class OpenSenseMapHumidity(Entity):
    """Implementation of an openSenseMap humidity entity."""

    def __init__(self, name, osm):
        """Initialize the humidity entity."""
        self._name = name
        self._osm = osm
        self._icon = "mdi:water-percent"
        self._unit_of_measurement = PERCENTAGE

    @property
    def icon(self):
        """Return the icon."""
        return self._icon

    @property
    def name(self):
        """Return the name of the humidity entity."""
        return f"{self._name}_humidity"

    @property
    def attribution(self):
        """Return the attribution."""
        return ATTRIBUTION

    @property
    def state(self):
        """Return the state of the device."""
        if self._osm.api.humidity is not None:
            return self._osm.api.humidity

    @property
    def unit_of_measurement(self):
        """Return the unit of measurement of this entity, if any."""
        return self._unit_of_measurement

    async def async_update(self):
        """Get the latest data from the openSenseMap API."""
        await self._osm.async_update()





class OpenSenseMapData:
    """Get the latest data and update the states."""

    def __init__(self, api):
        """Initialize the data object."""
        self.api = api

    @Throttle(SCAN_INTERVAL)
    async def async_update(self):
        """Get the latest data from the Pi-hole."""

        try:
            await self.api.get_data()
        except OpenSenseMapError as err:
            _LOGGER.error("Unable to fetch data: %s", err)
